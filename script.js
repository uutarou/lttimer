$(function() {
    //jsまじめに書くのはじめて過ぎて辛い。うううつらいよおおおおおおおおお
    var isRunning = false;
    var remainTime = 180; //残り時間…的な？（英語がこれでいいのかは全く不明）
    var intervalId = null;
    var timerElement = document.getElementById('timer');
    var socket = io.connect();

    document.onkeydown = keyPressed;

    function keyPressed() {
        switch (event.keyCode) {
            case 32: //スペースキーのキーコード
                console.log('Space pressed.');
                if (isRunning) {
                    stopTimer();
                } else {
                    startTimer();
                }
                break;
            case 82: //Rキーのキーコード
                console.log('R pressed.');
                resetTimer();
                break;
            default:
                console.log(event.keyCode);
        }
    }

    function startTimer() {
        console.log('timer started...');
        isRunning = true;
        socket.emit('timerStatus',isRunning);
        intervalId = setInterval(countDown, 1000);
    }

    function stopTimer() {
        console.log('timer stopped...');
        isRunning = false;
        socket.emit('timerStatus',isRunning);
        if (intervalId != null) {
            clearInterval(intervalId);
            intervalId = null;
        }
    }

    function resetTimer() {
        isRunning = false;
        socket.emit('timerStatus',isRunning);
        stopTimer();
        remainTime = 180;
        socket.emit('remainTime',remainTime);
        timerElement.innerHTML = secondToFormat(remainTime);
    }

    function countDown() {
        isRunning = true;
        remainTime = remainTime - 1;
        socket.emit('remainTime',remainTime);
        console.log(remainTime);
        timerElement.innerHTML = secondToFormat(remainTime);
    }

    function secondToFormat(time) {
        var minute = null;
        var second = null;

        minute = Math.floor(time / 60);
        if (time % 60 < 0) {
            second = (time % 60) * (-1);
        }else {
            second = time % 60;
        }

        if (second < 10) {
            second = '0' + second;
        }
        return minute + ':' + second;
        console.log(minute + ':' + second);
    }

    socket.on('control',function(data){
        switch (data) {
            case 'reset':
                resetTimer();
                break;
            case 'startStop':
                if (isRunning) {
                    stopTimer();
                }else {
                    startTimer();
                }
                break;
            default:
                console('謎の命令が来ました。');

        }
    });
});
