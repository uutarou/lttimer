var app = require('http').createServer(handler);
var io = require('socket.io')(app);
var fs = require('fs');
var settings = require('./settings');

app.listen(settings.port); //8080番ポートで待ち受け（という意味だと思う）
console.log('Server is running on port ' + settings.port + ' ...');

function handler(req, res) {
    fs.readFile(__dirname + req.url + '.html', function(err, data) {
        if (err) {
            res.writeHead(404);
            return res.end('404 NotFound.');
        }
        res.writeHead(200);
        res.write(data);
        res.end();
    });
}

io.sockets.on('connection', function(socket) {
    socket.on('timerStatus',function(data){
        //タイマーが動いてればtrue,そうでなければfalse
        console.log(data);
    });
    socket.on('remainTime',function(data){
        //残り時間が秒で渡ってくる
        console.log(data);
        io.emit('remainTime',data);
    });

    //controllerから飛んでくるやつ
    socket.on('control',function(data){
        switch (data) {
            case 'startStop':
                io.emit('control',data);
                break;
            case 'reset':
                io.emit('control',data);
                break;
            case 'bell':
                io.emit('control',data);
                break;
            default:
                console.log(data);
        }
    })
});
